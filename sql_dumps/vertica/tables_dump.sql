CREATE SCHEMA tungsten_alpha;
CREATE SCHEMA tercept_reports;


CREATE TABLE public.dfp_daily
(
    ID  IDENTITY ,
    CUSTOMER_ID int NOT NULL,
    MONTH varchar(20) DEFAULT NULL,
    MONTH_INDEX int DEFAULT NULL::int,
    WEEK varchar(20) DEFAULT NULL,
    YEAR varchar(20) DEFAULT NULL,
    DATE date NOT NULL,
    ORDER_ID int DEFAULT NULL::int,
    LINE_ITEM_ID int DEFAULT NULL::int,
    AD_UNIT_ID int DEFAULT NULL::int,
    CREATIVE_ID int DEFAULT NULL::int,
    ORDER_NAME varchar(256) DEFAULT NULL,
    LINE_ITEM_NAME varchar(256) DEFAULT NULL,
    AD_UNIT_NAME varchar(100) DEFAULT NULL,
    DEVICE_CATEGORY_NAME varchar(32) DEFAULT NULL,
    MOBILE_DEVICE_NAME varchar(32) DEFAULT NULL,
    MOBILE_INVENTORY_TYPE varchar(32) DEFAULT NULL,
    CREATIVE_NAME varchar(256) DEFAULT NULL,
    CREATIVE_SIZE varchar(32) DEFAULT NULL,
    CREATIVE_SIZE_DELIVERED varchar(32) DEFAULT NULL,
    AD_REQUEST_SIZE varchar(32) DEFAULT NULL,
    LINE_ITEM_PRIORITY int DEFAULT NULL::int,
    AD_SERVER_IMPRESSIONS int DEFAULT NULL::int,
    AD_EXCHANGE_IMPRESSIONS int DEFAULT NULL::int,
    AD_SERVER_CLICKS int DEFAULT NULL::int,
    AD_EXCHANGE_CLICKS int DEFAULT NULL::int,
    AD_SERVER_CPM_AND_CPC_REVENUE numeric(20,2) DEFAULT NULL::numeric(1,0),
    AD_EXCHANGE_ESTIMATED_REVENUE numeric(20,2) DEFAULT NULL::numeric(1,0),
    TOTAL_INVENTORY_LEVEL_UNFILLED_IMPRESSIONS int DEFAULT NULL::int
);

ALTER TABLE public.dfp_daily ADD CONSTRAINT C_PRIMARY PRIMARY KEY (ID) DISABLED;
ALTER TABLE public.dfp_daily ADD CONSTRAINT DFP_DAILY__UNIQUE_KEY__NORMAL_REPORT UNIQUE (CUSTOMER_ID, DATE, ORDER_ID, LINE_ITEM_ID, AD_UNIT_ID, CREATIVE_ID, DEVICE_CATEGORY_NAME, MOBILE_DEVICE_NAME, MOBILE_INVENTORY_TYPE, CREATIVE_SIZE, CREATIVE_SIZE_DELIVERED, LINE_ITEM_PRIORITY) DISABLED;
ALTER TABLE public.dfp_daily ADD CONSTRAINT DFP_DAILY__UNIQUE_KEY__UNFILLED_IMPRESSIONS UNIQUE (CUSTOMER_ID, DATE, AD_UNIT_ID, DEVICE_CATEGORY_NAME, MOBILE_DEVICE_NAME, MOBILE_INVENTORY_TYPE, AD_REQUEST_SIZE) DISABLED;

CREATE TABLE public.hotstar_hourly
(
    ID  IDENTITY ,
    CUSTOMER_ID varchar(5) NOT NULL,
    MONTH varchar(20) DEFAULT NULL,
    MONTH_INDEX varchar(3) DEFAULT NULL,
    WEEK varchar(20) DEFAULT NULL,
    HOUR time NOT NULL,
    DATE date NOT NULL,
    COUNTRY varchar(200) NOT NULL,
    ORDER_ID varchar(200) NOT NULL,
    LINE_ITEM varchar(200) NOT NULL,
    CREATIVE_SIZE varchar(18) NOT NULL,
    KEY_VALUE varchar(50) NOT NULL,
    AD_UNIT varchar(32) NOT NULL,
    ADVERTISER varchar(200) NOT NULL,
    AGG_DEMAND_CHANNEL varchar(25) NOT NULL,
    DEMAND_CHANNEL varchar(32) NOT NULL,
    LINE_ITEM_PRIORITY varchar(3) NOT NULL,
    TOTAL_IMPRESSIONS varchar(20) DEFAULT NULL,
    TOTAL_CLICKS varchar(20) DEFAULT NULL,
    TOTAL_CPM_CPC numeric(20,10) DEFAULT NULL::numeric(1,0),
    YEAR varchar(4)
);

ALTER TABLE public.hotstar_hourly ADD CONSTRAINT C_PRIMARY PRIMARY KEY (ID) DISABLED;

CREATE TABLE public.bidding_data
(
    ID  IDENTITY ,
    EXCHANGE_CODE varchar(10) NOT NULL DEFAULT 'ADX',
    CUSTOMER_ID numeric(20,0) NOT NULL DEFAULT 1,
    DATE date NOT NULL,
    MONTH varchar(20) DEFAULT NULL,
    MONTH_INDEX numeric(2,0),
    YEAR numeric(4,0) DEFAULT NULL::numeric(1,0),
    WEEK varchar(20) DEFAULT NULL,
    AD_TAG_NAME varchar(50) NOT NULL,
    WINNING_BID_RULE_NAME varchar(50) NOT NULL,
    COUNTRY_NAME varchar(100) NOT NULL,
    PLATFORM_TYPE_NAME varchar(50) NOT NULL,
    BRAND_NAME varchar(255) DEFAULT NULL,
    DSP_NAME varchar(255) NOT NULL,
    DEAL_NAME varchar(255) NOT NULL,
    BIDS numeric(20,0) DEFAULT NULL::numeric(1,0),
    AVERAGE_BID_CPM numeric(20,10) DEFAULT NULL::numeric(1,0),
    EARNINGS numeric(30,10) DEFAULT NULL::numeric(1,0),
    AD_IMPRESSIONS numeric(20,0) DEFAULT NULL::numeric(1,0),
    REF_WINNING_BID_CPM numeric(20,10) DEFAULT NULL::numeric(1,0),
    REF_CLOSE_CPM numeric(20,0) DEFAULT NULL::numeric(1,0),
    REF_WIN_PERCENTAGE varchar(50) DEFAULT NULL,
    EARNINGS_WINNING_BID numeric(30,10) DEFAULT NULL::numeric(1,0)
);

ALTER TABLE public.bidding_data ADD CONSTRAINT C_PRIMARY PRIMARY KEY (ID) DISABLED;
ALTER TABLE public.bidding_data ADD CONSTRAINT UK__BIDDING_DATA__ALL_KEYS UNIQUE (CUSTOMER_ID, DATE, AD_TAG_NAME, WINNING_BID_RULE_NAME, COUNTRY_NAME, PLATFORM_TYPE_NAME, BRAND_NAME, DSP_NAME, DEAL_NAME) ENABLED;

CREATE TABLE public.dfp_hourly
(
    ID  IDENTITY ,
    CUSTOMER_ID int NOT NULL,
    HOUR time NOT NULL,
    MONTH varchar(20) DEFAULT NULL,
    MONTH_INDEX numeric(2,0) DEFAULT NULL::numeric(1,0),
    WEEK varchar(20) DEFAULT NULL,
    YEAR varchar(20) DEFAULT NULL,
    DATE date NOT NULL,
    ORDER_ID numeric(20,0) DEFAULT NULL::numeric(1,0),
    LINE_ITEM_ID numeric(20,0) DEFAULT NULL::numeric(1,0),
    AD_UNIT_ID numeric(20,0) NOT NULL,
    CREATIVE_ID numeric(20,0) DEFAULT NULL::numeric(1,0),
    ORDER_NAME varchar(256) DEFAULT NULL,
    LINE_ITEM_NAME varchar(256) DEFAULT NULL,
    AD_UNIT_NAME varchar(100) NOT NULL,
    DEVICE_CATEGORY_NAME varchar(32) DEFAULT NULL,
    MOBILE_DEVICE_NAME varchar(32) DEFAULT NULL,
    MOBILE_INVENTORY_TYPE varchar(32) DEFAULT NULL,
    CREATIVE_NAME varchar(256) DEFAULT NULL,
    CREATIVE_SIZE varchar(32) DEFAULT NULL,
    CREATIVE_SIZE_DELIVERED varchar(32) DEFAULT NULL,
    AD_REQUEST_SIZE varchar(32) DEFAULT NULL,
    LINE_ITEM_PRIORITY numeric(3,0) DEFAULT NULL::numeric(1,0),
    AD_SERVER_IMPRESSIONS numeric(20,0) DEFAULT NULL::numeric(1,0),
    AD_EXCHANGE_IMPRESSIONS numeric(20,0) DEFAULT NULL::numeric(1,0),
    AD_SERVER_CLICKS numeric(20,0) DEFAULT NULL::numeric(1,0),
    AD_EXCHANGE_CLICKS numeric(20,0) DEFAULT NULL::numeric(1,0),
    AD_SERVER_CPM_AND_CPC_REVENUE numeric(20,2) DEFAULT NULL::numeric(1,0),
    AD_EXCHANGE_ESTIMATED_REVENUE numeric(20,2) DEFAULT NULL::numeric(1,0),
    TOTAL_INVENTORY_LEVEL_UNFILLED_IMPRESSIONS numeric(20,0) DEFAULT NULL::numeric(1,0)
);

ALTER TABLE public.dfp_hourly ADD CONSTRAINT C_PRIMARY PRIMARY KEY (ID) DISABLED;
ALTER TABLE public.dfp_hourly ADD CONSTRAINT DFP_HOURLY__UNIQUE_KEY__NORMAL_REPORT UNIQUE (CUSTOMER_ID, HOUR, DATE, ORDER_ID, LINE_ITEM_ID, AD_UNIT_ID, CREATIVE_ID, DEVICE_CATEGORY_NAME, MOBILE_DEVICE_NAME, MOBILE_INVENTORY_TYPE, CREATIVE_SIZE, CREATIVE_SIZE_DELIVERED, LINE_ITEM_PRIORITY) ENABLED;
ALTER TABLE public.dfp_hourly ADD CONSTRAINT DFP_HOURLY__UNIQUE_KEY__UNFILLED_IMPRESSIONS UNIQUE (CUSTOMER_ID, HOUR, DATE, AD_UNIT_ID, DEVICE_CATEGORY_NAME, MOBILE_DEVICE_NAME, MOBILE_INVENTORY_TYPE, AD_REQUEST_SIZE) ENABLED;

CREATE TABLE tungsten_alpha.trep_commit_seqno
(
    task_id int NOT NULL,
    seqno int,
    fragno int,
    last_frag char(5),
    source_id varchar(128),
    epoch_number int,
    eventid varchar(128),
    applied_latency int,
    update_timestamp timestamp,
    shard_id varchar(128),
    extract_timestamp timestamp
);


CREATE TABLE tungsten_alpha.consistency
(
    db char(64) NOT NULL,
    tbl char(64) NOT NULL,
    id int NOT NULL,
    row_offset int NOT NULL,
    row_limit int NOT NULL,
    this_crc char(40),
    this_cnt int,
    master_crc char(40),
    master_cnt int,
    ts timestamp,
    method char(32)
);


CREATE TABLE tungsten_alpha.heartbeat
(
    id int NOT NULL,
    seqno int,
    eventid varchar(128),
    source_tstamp timestamp,
    target_tstamp timestamp,
    lag_millis int,
    salt int,
    name varchar(128)
);


CREATE TABLE tungsten_alpha.stage_xxx_heartbeat
(
    tungsten_opcode varchar(2),
    tungsten_seqno int NOT NULL,
    tungsten_row_id int NOT NULL,
    tungsten_commit_timestamp timestamp,
    id int NOT NULL,
    seqno int,
    eventid varchar(128),
    source_tstamp timestamp,
    target_tstamp timestamp,
    lag_millis int,
    salt int,
    name varchar(128)
);


CREATE TABLE tungsten_alpha.trep_shard
(
    shard_id varchar(128) NOT NULL,
    master varchar(128),
    critical int
);


CREATE TABLE tungsten_alpha.trep_shard_channel
(
    shard_id varchar(128) NOT NULL,
    channel int
);


CREATE TABLE tercept_reports.alerts
(
    ALERT_ID numeric(38,0),
    CODE varchar(50),
    INFO varchar(65000),
    IS_FALSE_ALERT int,
    IS_KNOWN_ISSUE int,
    IS_FIXED int,
    CREATED_DATE_TIME timestamp,
    MODIFIED_DATE_TIME timestamp,
    CUSTOMER_ID numeric(38,0),
    ALERT_HASH varchar(100)
);


CREATE TABLE tercept_reports.bidding_data
(
    ID numeric(38,0),
    EXCHANGE_CODE varchar(10),
    CUSTOMER_ID numeric(38,0),
    DATE date,
    MONTH varchar(20),
    MONTH_INDEX int,
    YEAR varchar(20),
    WEEK varchar(20),
    AD_TAG_NAME varchar(50),
    WINNING_BID_RULE_NAME varchar(50),
    COUNTRY_NAME varchar(100),
    PLATFORM_TYPE_NAME varchar(50),
    BRAND_NAME varchar(255),
    DSP_NAME varchar(255),
    DEAL_NAME varchar(255),
    BIDS varchar(40),
    AVERAGE_BID_CPM float,
    EARNINGS float,
    AD_IMPRESSIONS varchar(40),
    REF_WINNING_BID_CPM float,
    REF_CLOSE_CPM numeric(38,0),
    REF_WIN_PERCENTAGE varchar(50),
    EARNINGS_WINNING_BID float
);


CREATE TABLE tercept_reports.calendar_table
(
    dt date,
    y int,
    q int,
    m int,
    d int,
    dw int,
    monthName varchar(9),
    dayName varchar(9),
    w int,
    isWeekday binary(1),
    isHoliday binary(1),
    holidayDescr varchar(32),
    isPayday binary(1)
);


CREATE TABLE tercept_reports.customer__alerts
(
    ID numeric(38,0),
    CUSTOMER_ID numeric(38,0),
    MONITOR_TYPE_ID numeric(38,0),
    MONITOR_GROUP_ID numeric(38,0),
    EXCHANGE_ID numeric(38,0),
    DROP_THRESHOLD float,
    JUMP_THRESHOLD float,
    ALERT_TYPE int,
    CONTROL_DURATION int,
    TEST_DURATION int,
    LAST_ALERTED timestamp,
    CREATED timestamp,
    IS_ENABLED int,
    FILTER numeric(38,0)
);


CREATE TABLE tercept_reports.customers
(
    CUSTOMER_ID numeric(38,0) NOT NULL,
    CUSTOMER_NAME varchar(100),
    EMAIL varchar(100),
    PASSWORD varchar(100),
    IS_VERIFIED int,
    REPORT_EMAIL varchar(100),
    IS_ENABLED int,
    ADX_REFRESH_TOKEN varchar(200),
    HOURLY_CSV_FOLDER varchar(100),
    HOURLY_GZIP_FOLDER varchar(100),
    DAILY_CSV_FOLDER varchar(100),
    DAILY_GZIP_FOLDER varchar(100),
    CREDENTIAL_PATH varchar(100),
    IS_REPORTED int,
    ADX_EMAIL varchar(50),
    ADX_PASSWORD varchar(50),
    CREATED timestamp,
    DFP_EMAIL varchar(50),
    DFP_PASSWORD varchar(50),
    DFP_REFRESH_TOKEN varchar(200),
    NETWORK_CODE varchar(20)
);

ALTER TABLE tercept_reports.customers ADD CONSTRAINT C_PRIMARY PRIMARY KEY (CUSTOMER_ID) DISABLED;

CREATE TABLE tercept_reports.exchange_data
(
    ID numeric(38,0),
    CUSTOMER_ID numeric(38,0),
    EXCHANGE_CODE varchar(4),
    DATE date,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX int,
    YEAR varchar(20),
    AD_TAG_NAME varchar(50),
    COUNTRY_NAME varchar(40),
    PLATFORM_TYPE_NAME varchar(30),
    DSP_NAME varchar(100),
    BUYER_NETWORK_NAME varchar(255),
    BRAND_NAME varchar(255),
    BRANDING_TYPE_NAME varchar(30),
    WINNING_BID_RULE_NAME varchar(50),
    DEAL_NAME varchar(150),
    CREATIVE_SIZE varchar(255),
    TRANSACTION_TYPE_NAME varchar(20),
    AD_REQUESTS numeric(38,0),
    AD_IMPRESSIONS numeric(38,0),
    CLICKS numeric(38,0),
    EARNINGS float,
    AD_IMPRESSIONS_RPM float,
    ACTIVE_VIEW_VIEWABLE_COUNT numeric(38,0),
    ACTIVE_VIEW_MEASURABLE_COUNT numeric(38,0)
);


CREATE TABLE tercept_reports.log
(
    "time" timestamp,
    log varchar(65000)
);


CREATE TABLE tercept_reports.monitor_groups
(
    ID numeric(38,0),
    GROUP_NAME varchar(50),
    DESCRIPTION varchar(255),
    PARAMS varchar(255)
);


CREATE TABLE tercept_reports.monitor_types
(
    ID numeric(38,0),
    MONITOR_NAME varchar(50),
    DESCRIPTION varchar(255),
    PARAMS varchar(255),
    FILTER varchar(100),
    HOURLY_FILTER varchar(100)
);


CREATE TABLE tercept_reports.pricing_rule
(
    id numeric(38,0),
    customer_id numeric(38,0),
    name varchar(100),
    pricing_stage varchar(20),
    tar_tags varchar(1000),
    tar_dfp_ad_unit varchar(1000),
    tar_dfp_placement varchar(1000),
    tar_url varchar(500),
    tar_geo varchar(1000),
    tar_inventory_size varchar(500),
    tar_dfp_keyvalues varchar(500),
    tar_audience varchar(1000),
    tar_os varchar(200),
    tar_device_category varchar(200),
    created timestamp,
    last_active date,
    not_changed_count int,
    sha1_hash varchar(40)
);


CREATE TABLE tercept_reports.qrtz_blob_triggers
(
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    BLOB_DATA varbinary(65000)
);


CREATE TABLE tercept_reports.qrtz_calendars
(
    SCHED_NAME varchar(120),
    CALENDAR_NAME varchar(200),
    CALENDAR varbinary(65000)
);


CREATE TABLE tercept_reports.qrtz_cron_triggers
(
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    CRON_EXPRESSION varchar(120),
    TIME_ZONE_ID varchar(80)
);


CREATE TABLE tercept_reports.qrtz_fired_triggers
(
    SCHED_NAME varchar(120),
    ENTRY_ID varchar(95),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    INSTANCE_NAME varchar(200),
    FIRED_TIME numeric(38,0),
    SCHED_TIME numeric(38,0),
    PRIORITY int,
    STATE varchar(16),
    JOB_NAME varchar(200),
    JOB_GROUP varchar(200),
    IS_NONCONCURRENT varchar(1),
    REQUESTS_RECOVERY varchar(1)
);


CREATE TABLE tercept_reports.qrtz_job_details
(
    SCHED_NAME varchar(120),
    JOB_NAME varchar(200),
    JOB_GROUP varchar(200),
    DESCRIPTION varchar(250),
    JOB_CLASS_NAME varchar(250),
    IS_DURABLE varchar(1),
    IS_NONCONCURRENT varchar(1),
    IS_UPDATE_DATA varchar(1),
    REQUESTS_RECOVERY varchar(1),
    JOB_DATA varbinary(65000)
);


CREATE TABLE tercept_reports.qrtz_locks
(
    SCHED_NAME varchar(120),
    LOCK_NAME varchar(40)
);


CREATE TABLE tercept_reports.qrtz_paused_trigger_grps
(
    SCHED_NAME varchar(120),
    TRIGGER_GROUP varchar(200)
);


CREATE TABLE tercept_reports.qrtz_scheduler_state
(
    SCHED_NAME varchar(120),
    INSTANCE_NAME varchar(200),
    LAST_CHECKIN_TIME numeric(38,0),
    CHECKIN_INTERVAL numeric(38,0)
);


CREATE TABLE tercept_reports.qrtz_simple_triggers
(
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    REPEAT_COUNT numeric(38,0),
    REPEAT_INTERVAL numeric(38,0),
    TIMES_TRIGGERED numeric(38,0)
);


CREATE TABLE tercept_reports.qrtz_simprop_triggers
(
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    STR_PROP_1 varchar(512),
    STR_PROP_2 varchar(512),
    STR_PROP_3 varchar(512),
    INT_PROP_1 int,
    INT_PROP_2 int,
    LONG_PROP_1 numeric(38,0),
    LONG_PROP_2 numeric(38,0),
    DEC_PROP_1 numeric(13,4),
    DEC_PROP_2 numeric(13,4),
    BOOL_PROP_1 varchar(1),
    BOOL_PROP_2 varchar(1)
);


CREATE TABLE tercept_reports.qrtz_triggers
(
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    JOB_NAME varchar(200),
    JOB_GROUP varchar(200),
    DESCRIPTION varchar(250),
    NEXT_FIRE_TIME numeric(38,0),
    PREV_FIRE_TIME numeric(38,0),
    PRIORITY int,
    TRIGGER_STATE varchar(16),
    TRIGGER_TYPE varchar(8),
    START_TIME numeric(38,0),
    END_TIME numeric(38,0),
    CALENDAR_NAME varchar(200),
    MISFIRE_INSTR int,
    JOB_DATA varbinary(65000)
);


CREATE TABLE tercept_reports.user_roles
(
    user_role_id int,
    username varchar(45),
    user_id int,
    ROLE varchar(45)
);


CREATE TABLE tercept_reports.users
(
    user_id int,
    username varchar(45),
    password varchar(100),
    email varchar(100),
    enabled int,
    customer_id int
);


CREATE TABLE tercept_reports.stage_xxx_alerts
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ALERT_ID numeric(38,0),
    CODE varchar(50),
    INFO varchar(65000),
    IS_FALSE_ALERT int,
    IS_KNOWN_ISSUE int,
    IS_FIXED int,
    CREATED_DATE_TIME timestamp,
    MODIFIED_DATE_TIME timestamp,
    CUSTOMER_ID numeric(38,0),
    ALERT_HASH varchar(100)
);


CREATE TABLE tercept_reports.stage_xxx_bidding_data
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    EXCHANGE_CODE varchar(10),
    CUSTOMER_ID numeric(38,0),
    DATE date,
    MONTH varchar(20),
    MONTH_INDEX int,
    YEAR varchar(20),
    WEEK varchar(20),
    AD_TAG_NAME varchar(50),
    WINNING_BID_RULE_NAME varchar(50),
    COUNTRY_NAME varchar(100),
    PLATFORM_TYPE_NAME varchar(50),
    BRAND_NAME varchar(255),
    DSP_NAME varchar(255),
    DEAL_NAME varchar(255),
    BIDS varchar(40),
    AVERAGE_BID_CPM float,
    EARNINGS float,
    AD_IMPRESSIONS varchar(40),
    REF_WINNING_BID_CPM float,
    REF_CLOSE_CPM numeric(38,0),
    REF_WIN_PERCENTAGE varchar(50),
    EARNINGS_WINNING_BID float
);


CREATE TABLE tercept_reports.stage_xxx_calendar_table
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    dt date,
    y int,
    q int,
    m int,
    d int,
    dw int,
    monthName varchar(9),
    dayName varchar(9),
    w int,
    isWeekday binary(1),
    isHoliday binary(1),
    holidayDescr varchar(32),
    isPayday binary(1)
);


CREATE TABLE tercept_reports.stage_xxx_customer__alerts
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    CUSTOMER_ID numeric(38,0),
    MONITOR_TYPE_ID numeric(38,0),
    MONITOR_GROUP_ID numeric(38,0),
    EXCHANGE_ID numeric(38,0),
    DROP_THRESHOLD float,
    JUMP_THRESHOLD float,
    ALERT_TYPE int,
    CONTROL_DURATION int,
    TEST_DURATION int,
    LAST_ALERTED timestamp,
    CREATED timestamp,
    IS_ENABLED int,
    FILTER numeric(38,0)
);


CREATE TABLE tercept_reports.stage_xxx_customers
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    CUSTOMER_ID numeric(38,0),
    CUSTOMER_NAME varchar(100),
    EMAIL varchar(100),
    PASSWORD varchar(100),
    IS_VERIFIED int,
    REPORT_EMAIL varchar(100),
    IS_ENABLED int,
    ADX_REFRESH_TOKEN varchar(200),
    HOURLY_CSV_FOLDER varchar(100),
    HOURLY_GZIP_FOLDER varchar(100),
    DAILY_CSV_FOLDER varchar(100),
    DAILY_GZIP_FOLDER varchar(100),
    CREDENTIAL_PATH varchar(100),
    IS_REPORTED int,
    ADX_EMAIL varchar(50),
    ADX_PASSWORD varchar(50),
    CREATED timestamp,
    DFP_EMAIL varchar(50),
    DFP_PASSWORD varchar(50),
    DFP_REFRESH_TOKEN varchar(200),
    NETWORK_CODE varchar(20)
);


CREATE TABLE tercept_reports.stage_xxx_dfp_change_history
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    CUSTOMER_ID numeric(38,0),
    CHANGE_DATETIME timestamp,
    ENTITY_ID numeric(38,0),
    ENTITY_TYPE varchar(32),
    OPERATION varchar(32),
    USER_ID numeric(38,0)
);


CREATE TABLE tercept_reports.stage_xxx_dfp_daily
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    CUSTOMER_ID numeric(38,0),
    MONTH varchar(20),
    MONTH_INDEX int,
    WEEK varchar(20),
    YEAR varchar(20),
    DATE date,
    ORDER_ID numeric(38,0),
    LINE_ITEM_ID numeric(38,0),
    AD_UNIT_ID numeric(38,0),
    CREATIVE_ID numeric(38,0),
    ORDER_NAME varchar(256),
    LINE_ITEM_NAME varchar(256),
    AD_UNIT_NAME varchar(100),
    DEVICE_CATEGORY_NAME varchar(32),
    MOBILE_DEVICE_NAME varchar(32),
    MOBILE_INVENTORY_TYPE varchar(32),
    CREATIVE_NAME varchar(256),
    CREATIVE_SIZE varchar(32),
    CREATIVE_SIZE_DELIVERED varchar(32),
    AD_REQUEST_SIZE varchar(32),
    LINE_ITEM_PRIORITY int,
    AD_SERVER_IMPRESSIONS numeric(38,0),
    AD_EXCHANGE_IMPRESSIONS numeric(38,0),
    AD_SERVER_CLICKS numeric(38,0),
    AD_EXCHANGE_CLICKS numeric(38,0),
    AD_SERVER_CPM_AND_CPC_REVENUE numeric(20,2),
    AD_EXCHANGE_ESTIMATED_REVENUE numeric(20,2),
    TOTAL_INVENTORY_LEVEL_UNFILLED_IMPRESSIONS numeric(38,0)
);


CREATE TABLE tercept_reports.stage_xxx_dfp_hourly
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    CUSTOMER_ID numeric(38,0),
    HOUR time,
    MONTH varchar(20),
    MONTH_INDEX int,
    WEEK varchar(20),
    YEAR varchar(20),
    DATE date,
    ORDER_ID numeric(38,0),
    LINE_ITEM_ID numeric(38,0),
    AD_UNIT_ID numeric(38,0),
    CREATIVE_ID numeric(38,0),
    ORDER_NAME varchar(256),
    LINE_ITEM_NAME varchar(256),
    AD_UNIT_NAME varchar(100),
    DEVICE_CATEGORY_NAME varchar(32),
    MOBILE_DEVICE_NAME varchar(32),
    MOBILE_INVENTORY_TYPE varchar(32),
    CREATIVE_NAME varchar(256),
    CREATIVE_SIZE varchar(32),
    CREATIVE_SIZE_DELIVERED varchar(32),
    AD_REQUEST_SIZE varchar(32),
    LINE_ITEM_PRIORITY int,
    AD_SERVER_IMPRESSIONS numeric(38,0),
    AD_EXCHANGE_IMPRESSIONS numeric(38,0),
    AD_SERVER_CLICKS numeric(38,0),
    AD_EXCHANGE_CLICKS numeric(38,0),
    AD_SERVER_CPM_AND_CPC_REVENUE numeric(20,2),
    AD_EXCHANGE_ESTIMATED_REVENUE numeric(20,2),
    TOTAL_INVENTORY_LEVEL_UNFILLED_IMPRESSIONS numeric(38,0)
);


CREATE TABLE tercept_reports.stage_xxx_exchange_data
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    CUSTOMER_ID numeric(38,0),
    EXCHANGE_CODE varchar(4),
    DATE date,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX int,
    YEAR varchar(20),
    AD_TAG_NAME varchar(50),
    COUNTRY_NAME varchar(40),
    PLATFORM_TYPE_NAME varchar(30),
    DSP_NAME varchar(100),
    BUYER_NETWORK_NAME varchar(255),
    BRAND_NAME varchar(255),
    BRANDING_TYPE_NAME varchar(30),
    WINNING_BID_RULE_NAME varchar(50),
    DEAL_NAME varchar(150),
    CREATIVE_SIZE varchar(255),
    TRANSACTION_TYPE_NAME varchar(20),
    AD_REQUESTS numeric(38,0),
    AD_IMPRESSIONS numeric(38,0),
    CLICKS numeric(38,0),
    EARNINGS float,
    AD_IMPRESSIONS_RPM float,
    ACTIVE_VIEW_VIEWABLE_COUNT numeric(38,0),
    ACTIVE_VIEW_MEASURABLE_COUNT numeric(38,0)
);


CREATE TABLE tercept_reports.stage_xxx_exchange_hourly
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    CUSTOMER_ID numeric(38,0),
    EXCHANGE_CODE varchar(4),
    DATE date,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX int,
    YEAR varchar(20),
    AD_TAG_NAME varchar(50),
    COUNTRY_NAME varchar(40),
    PLATFORM_TYPE_NAME varchar(30),
    DSP_NAME varchar(100),
    BUYER_NETWORK_NAME varchar(255),
    BRAND_NAME varchar(255),
    BRANDING_TYPE_NAME varchar(30),
    WINNING_BID_RULE_NAME varchar(50),
    DEAL_NAME varchar(150),
    CREATIVE_SIZE varchar(255),
    HOUR time,
    TRANSACTION_TYPE_NAME varchar(20),
    AD_REQUESTS numeric(38,0),
    AD_IMPRESSIONS numeric(38,0),
    CLICKS numeric(38,0),
    EARNINGS float,
    AD_IMPRESSIONS_RPM float,
    ACTIVE_VIEW_VIEWABLE_COUNT numeric(38,0),
    ACTIVE_VIEW_MEASURABLE_COUNT numeric(38,0)
);


CREATE TABLE tercept_reports.stage_xxx_log
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    "time" timestamp,
    log varchar(65000)
);


CREATE TABLE tercept_reports.stage_xxx_monitor_groups
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    GROUP_NAME varchar(50),
    DESCRIPTION varchar(255),
    PARAMS varchar(255)
);


CREATE TABLE tercept_reports.stage_xxx_monitor_types
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    ID numeric(38,0),
    MONITOR_NAME varchar(50),
    DESCRIPTION varchar(255),
    PARAMS varchar(255),
    FILTER varchar(100),
    HOURLY_FILTER varchar(100)
);


CREATE TABLE tercept_reports.stage_xxx_pricing_rule
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    id numeric(38,0),
    customer_id numeric(38,0),
    name varchar(100),
    pricing_stage varchar(20),
    tar_tags varchar(1000),
    tar_dfp_ad_unit varchar(1000),
    tar_dfp_placement varchar(1000),
    tar_url varchar(500),
    tar_geo varchar(1000),
    tar_inventory_size varchar(500),
    tar_dfp_keyvalues varchar(500),
    tar_audience varchar(1000),
    tar_os varchar(200),
    tar_device_category varchar(200),
    created timestamp,
    last_active date,
    not_changed_count int,
    sha1_hash varchar(40)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_blob_triggers
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    BLOB_DATA varbinary(65000)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_calendars
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    CALENDAR_NAME varchar(200),
    CALENDAR varbinary(65000)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_cron_triggers
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    CRON_EXPRESSION varchar(120),
    TIME_ZONE_ID varchar(80)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_fired_triggers
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    ENTRY_ID varchar(95),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    INSTANCE_NAME varchar(200),
    FIRED_TIME numeric(38,0),
    SCHED_TIME numeric(38,0),
    PRIORITY int,
    STATE varchar(16),
    JOB_NAME varchar(200),
    JOB_GROUP varchar(200),
    IS_NONCONCURRENT varchar(1),
    REQUESTS_RECOVERY varchar(1)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_job_details
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    JOB_NAME varchar(200),
    JOB_GROUP varchar(200),
    DESCRIPTION varchar(250),
    JOB_CLASS_NAME varchar(250),
    IS_DURABLE varchar(1),
    IS_NONCONCURRENT varchar(1),
    IS_UPDATE_DATA varchar(1),
    REQUESTS_RECOVERY varchar(1),
    JOB_DATA varbinary(65000)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_locks
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    LOCK_NAME varchar(40)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_paused_trigger_grps
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    TRIGGER_GROUP varchar(200)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_scheduler_state
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    INSTANCE_NAME varchar(200),
    LAST_CHECKIN_TIME numeric(38,0),
    CHECKIN_INTERVAL numeric(38,0)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_simple_triggers
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    REPEAT_COUNT numeric(38,0),
    REPEAT_INTERVAL numeric(38,0),
    TIMES_TRIGGERED numeric(38,0)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_simprop_triggers
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    STR_PROP_1 varchar(512),
    STR_PROP_2 varchar(512),
    STR_PROP_3 varchar(512),
    INT_PROP_1 int,
    INT_PROP_2 int,
    LONG_PROP_1 numeric(38,0),
    LONG_PROP_2 numeric(38,0),
    DEC_PROP_1 numeric(13,4),
    DEC_PROP_2 numeric(13,4),
    BOOL_PROP_1 varchar(1),
    BOOL_PROP_2 varchar(1)
);


CREATE TABLE tercept_reports.stage_xxx_qrtz_triggers
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    SCHED_NAME varchar(120),
    TRIGGER_NAME varchar(200),
    TRIGGER_GROUP varchar(200),
    JOB_NAME varchar(200),
    JOB_GROUP varchar(200),
    DESCRIPTION varchar(250),
    NEXT_FIRE_TIME numeric(38,0),
    PREV_FIRE_TIME numeric(38,0),
    PRIORITY int,
    TRIGGER_STATE varchar(16),
    TRIGGER_TYPE varchar(8),
    START_TIME numeric(38,0),
    END_TIME numeric(38,0),
    CALENDAR_NAME varchar(200),
    MISFIRE_INSTR int,
    JOB_DATA varbinary(65000)
);


CREATE TABLE tercept_reports.stage_xxx_user_roles
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    user_role_id int,
    username varchar(45),
    user_id int,
    ROLE varchar(45)
);


CREATE TABLE tercept_reports.stage_xxx_users
(
    tungsten_opcode char(1),
    tungsten_seqno int,
    tungsten_row_id int,
    tungsten_commit_timestamp timestamp,
    user_id int,
    username varchar(45),
    password varchar(100),
    email varchar(100),
    enabled int,
    customer_id int
);


CREATE TABLE tercept_reports.pubmatic_nodeal
(
    DATE date NOT NULL,
    CUSTOMER_ID numeric(38,0) NOT NULL,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR varchar(20),
    AD_TAG_NAME varchar(50),
    COUNTRY_NAME varchar(40),
    PLATFORM_TYPE varchar(30),
    DSP_NAME varchar(100),
    CHANNEL_NAME varchar(30),
    AD_SIZE varchar(40),
    FOLD_POSITION varchar(20),
    ADVERTISER_NAME varchar(60),
    AD_REQUESTS numeric(38,0),
    AD_IMPRESSIONS numeric(38,0),
    CLICKS numeric(38,0),
    EARNINGS numeric(20,10),
    PB_IMPRESSIONS_NET numeric(38,0),
    PB_IMPRESSIONS_NONAPPROVED numeric(38,0),
    DEFAULTS numeric(38,0),
    PBIMPRESSIONSBOT numeric(38,0)
)
PARTITION BY (((pubmatic_nodeal.CUSTOMER_ID * 100) + date_part('month', pubmatic_nodeal.DATE)));

ALTER TABLE tercept_reports.pubmatic_nodeal ADD CONSTRAINT PUB__NODEAL__UNIQUE__KEY UNIQUE (DATE, CUSTOMER_ID, AD_TAG_NAME, COUNTRY_NAME, PLATFORM_TYPE, DSP_NAME, CHANNEL_NAME, AD_SIZE, FOLD_POSITION, ADVERTISER_NAME) ENABLED;

CREATE TABLE tercept_reports.pubmatic_deal
(
    DATE date NOT NULL,
    CUSTOMER_ID numeric(38,0) NOT NULL,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR varchar(20),
    CHANNEL_NAME varchar(30),
    PLATFORM_TYPE varchar(30),
    AD_SIZE varchar(40),
    FOLD_POSITION varchar(20),
    DEAL_NAME varchar(60),
    ADVERTISER_NAME varchar(60),
    DSP_NAME varchar(100),
    AD_REQUESTS numeric(38,0),
    AD_IMPRESSIONS numeric(38,0),
    EARNINGS numeric(20,10),
    CLICKS numeric(38,0),
    PB_IMPRESSIONS_NET numeric(38,0),
    PB_IMPRESSIONS_NONAPPROVED numeric(38,0),
    DEFAULTS numeric(38,0),
    PBIMPRESSIONSBOT numeric(38,0)
)
PARTITION BY (((pubmatic_deal.CUSTOMER_ID * 100) + date_part('month', pubmatic_deal.DATE)));

ALTER TABLE tercept_reports.pubmatic_deal ADD CONSTRAINT PUB__DEAL__UNIQUE__KEY UNIQUE (DATE, CUSTOMER_ID, CHANNEL_NAME, PLATFORM_TYPE, AD_SIZE, FOLD_POSITION, DEAL_NAME, ADVERTISER_NAME, DSP_NAME) ENABLED;

CREATE TABLE tercept_reports.exchange_hourly
(
    DATE date NOT NULL,
    HOUR time NOT NULL,
    CUSTOMER_ID numeric(38,0) NOT NULL,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR varchar(20),
    PARTNER_ID varchar(6) NOT NULL,
    TRANSACTION_TYPE_NAME varchar(20),
    BRANDING_TYPE_NAME varchar(30),
    AD_TAG_NAME varchar(50),
    COUNTRY_NAME varchar(40),
    PLATFORM_TYPE_NAME varchar(30),
    DSP_NAME varchar(80),
    BRAND_NAME varchar(200),
    WINNING_BID_RULE_NAME varchar(80),
    DEAL_NAME varchar(116),
    AD_UNIT_SIZE_NAME varchar(116),
    AD_REQUESTS numeric(38,0),
    AD_IMPRESSIONS numeric(38,0),
    CLICKS numeric(38,0),
    AD_IMPRESSIONS_RPM numeric(20,10),
    EARNINGS numeric(20,10),
    ACTIVE_VIEW_VIEWABLE_COUNT numeric(38,0),
    ACTIVE_VIEW_MEASURABLE_COUNT numeric(38,0),
    MATCHED_AD_REQUESTS numeric(38,0),
    CREATIVE_SIZE varchar(116),
    ACCOUNT_ID numeric(38,0) NOT NULL,
    DFP_AD_UNITS varchar(116),
    INTEGRATION varchar(18),
    ADVERTISER_NAME varchar(255),
    DFP_REQUEST_SOURCE varchar(255)
)
PARTITION BY (((exchange_hourly.CUSTOMER_ID * 100) + date_part('month', exchange_hourly.DATE)));


CREATE TABLE tercept_reports.Programmatic_cube
(
    DATE date,
    CUSTOMER_ID numeric(38,0),
    EXCHANGE_CODE varchar(6),
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR varchar(20),
    SIZE_DELIVERED_CODE numeric(38,0),
    BRAND_CODE numeric(38,0),
    AUCTION_CODE numeric(38,0),
    DSP_CODE numeric(38,0),
    COUNTRY_CODE numeric(38,0),
    AD_REQUEST numeric(38,0),
    IMPRESSIONS numeric(38,0),
    CLICKS numeric(38,0),
    EARNINGS numeric(20,10)
);


CREATE TABLE tercept_reports.master_auction_type_table
(
    AUCTION_CODE numeric(38,0) NOT NULL,
    STANDARD_NAME varchar(150) DEFAULT NULL,
    ADX_NAME varchar(150) DEFAULT NULL,
    DFP_NAME varchar(150) DEFAULT NULL,
    PUB_NAME varchar(150) DEFAULT NULL,
    IS_MAPPED boolean DEFAULT NULL::boolean
);

ALTER TABLE tercept_reports.master_auction_type_table ADD CONSTRAINT C_PRIMARY PRIMARY KEY (AUCTION_CODE) DISABLED;

CREATE TABLE tercept_reports.master_size_delivered_table
(
    SIZE_DELIVERED_CODE numeric(38,0) NOT NULL,
    STANDARD_NAME varchar(150) DEFAULT NULL,
    ADX_NAME varchar(150) DEFAULT NULL,
    DFP_NAME varchar(150) DEFAULT NULL,
    PUB_NAME varchar(150) DEFAULT NULL,
    IS_MAPPED boolean DEFAULT NULL::boolean
);

ALTER TABLE tercept_reports.master_size_delivered_table ADD CONSTRAINT C_PRIMARY PRIMARY KEY (SIZE_DELIVERED_CODE) DISABLED;

CREATE TABLE tercept_reports.master_dsp_table
(
    DSP_CODE numeric(38,0) NOT NULL,
    STANDARD_NAME varchar(255) DEFAULT NULL,
    ADX_NAME varchar(255) DEFAULT NULL,
    DFP_NAME varchar(255) DEFAULT NULL,
    PUB_NAME varchar(255) DEFAULT NULL,
    IS_MAPPED boolean DEFAULT NULL::boolean
);

ALTER TABLE tercept_reports.master_dsp_table ADD CONSTRAINT C_PRIMARY PRIMARY KEY (DSP_CODE) DISABLED;

CREATE TABLE tercept_reports.master_country_table
(
    COUNTRY_CODE numeric(38,0) NOT NULL,
    STANDARD_NAME varchar(60) DEFAULT NULL,
    ADX_NAME varchar(60) DEFAULT NULL,
    DFP_NAME varchar(60) DEFAULT NULL,
    PUB_NAME varchar(60) DEFAULT NULL,
    IS_MAPPED boolean DEFAULT NULL::boolean
);

ALTER TABLE tercept_reports.master_country_table ADD CONSTRAINT C_PRIMARY PRIMARY KEY (COUNTRY_CODE) DISABLED;

CREATE TABLE tercept_reports.master_brand_table
(
    BRAND_CODE numeric(38,0) NOT NULL,
    STANDARD_NAME varchar(255) DEFAULT NULL,
    ADX_NAME varchar(255) DEFAULT NULL,
    DFP_NAME varchar(255) DEFAULT NULL,
    PUB_NAME varchar(255) DEFAULT NULL,
    IS_MAPPED boolean DEFAULT NULL::boolean
);

ALTER TABLE tercept_reports.master_brand_table ADD CONSTRAINT C_PRIMARY PRIMARY KEY (BRAND_CODE) DISABLED;

CREATE TABLE tercept_reports.google_adsense_hourly
(
    DATE date NOT NULL,
    HOUR time,
    CUSTOMER_ID numeric(38,0) NOT NULL,
    ACCOUNT_ID numeric(38,0) NOT NULL,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR varchar(20),
    PARTNER_CODE varchar(6),
    INTEGRATION varchar(18),
    AD_UNIT_NAME varchar(60),
    AD_UNIT_SIZE_NAME varchar(60),
    PLATFORM_TYPE_NAME varchar(50),
    COUNTRY_NAME varchar(66),
    AD_FORMAT_NAME varchar(40),
    AD_REQUESTS numeric(38,0),
    CLICKS numeric(38,0),
    EARNINGS numeric(20,10),
    INDIVIDUAL_AD_IMPRESSIONS numeric(38,0),
    MATCHED_AD_REQUESTS numeric(38,0),
    PAGE_VIEWS numeric(38,0)
)
PARTITION BY (((google_adsense_hourly.ACCOUNT_ID * 100) + date_part('month', google_adsense_hourly.DATE)));

ALTER TABLE tercept_reports.google_adsense_hourly ADD COLUMN PRODUCT_NAME varchar(60) DEFAULT NULL;
ALTER TABLE tercept_reports.google_adsense_hourly ADD COLUMN TARGETING_TYPE_NAME varchar(60) DEFAULT NULL;
alter table tercept_reports.google_adsense_hourly add column ACTIVE_VIEW_VIEWABLE_COUNT numeric(20,10) default null;
alter table tercept_reports.google_adsense_hourly add column AD_IMPRESSIONS_RPM numeric(20,10) default null;

CREATE TABLE tercept_reports.dfp_nogeo
(
    CUSTOMER_ID numeric(38,0) NOT NULL,
    DATE date NOT NULL,
    HOUR time NOT NULL,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR varchar(20),
    ORDER_NAME varchar(256),
    LINE_ITEM_NAME varchar(256),
    AD_UNIT_NAME varchar(100),
    DEVICE_CATEGORY_NAME varchar(32),
    MOBILE_DEVICE_NAME varchar(32),
    MOBILE_INVENTORY_TYPE varchar(32),
    CREATIVE_NAME varchar(256),
    REQUESTED_AD_SIZES varchar(60),
    CREATIVE_SIZE_DELIVERED varchar(32),
    ORDER_ID varchar(28),
    LINE_ITEM_ID varchar(28),
    AD_UNIT_ID varchar(28),
    DEVICE_CATEGORY_ID varchar(28),
    CREATIVE_ID varchar(28),
    LINE_ITEM_PRIORITY numeric(38,0),
    AD_SERVER_IMPRESSIONS numeric(38,0) DEFAULT 0,
    AD_EXCHANGE_IMPRESSIONS numeric(38,0) DEFAULT 0,
    AD_SERVER_CLICKS numeric(38,0) DEFAULT 0,
    AD_EXCHANGE_CLICKS numeric(38,0) DEFAULT 0,
    AD_EXCHANGE_ESTIMATED_REVENUE numeric(20,10),
    AD_SERVER_CPM_AND_CPC_REVENUE numeric(20,10),
    TOTAL_INVENTORY_LEVEL_UNFILLED_IMPRESSIONS numeric(38,0) DEFAULT 0,
    TOTAL_CODE_SERVED_COUNT numeric(38,0),
    INTEGRATION varchar(30) NOT NULL,
    ACCOUNT_ID numeric(38,0) NOT NULL,
    PLACEMENT_NAME varchar(255),
    PLACEMENT_ID varchar(28),
    COUNTRY_NAME varchar(60),
    CUSTOM_CRITERIA varchar(60),
    COUNTRY_CRITERIA_ID varchar(60),
    CUSTOM_TARGETING_VALUE_ID varchar(60),
    ADSENSE_LINE_ITEM_LEVEL_IMPRESSIONS numeric(38,0),
    ADSENSE_LINE_ITEM_LEVEL_CLICKS numeric(38,0),
    ADSENSE_LINE_ITEM_LEVEL_REVENUE numeric(20,10)
)
PARTITION BY (((dfp_nogeo.CUSTOMER_ID * 100) + date_part('month', dfp_nogeo.DATE)));
alter table tercept_reports.dfp_nogeo add column SDK_MEDIATION_CREATIVE_IMPRESSIONS numeric default null;
alter table tercept_reports.dfp_nogeo add column SDK_MEDIATION_CREATIVE_CLICKS numeric default null;
alter table tercept_reports.dfp_nogeo add column AD_SERVER_DOWNLOADED_IMPRESSIONS numeric default null;
alter table tercept_reports.dfp_nogeo add column AD_SERVER_ACTIVE_VIEW_VIEWABLE_IMPRESSIONS numeric default null;
alter table tercept_reports.dfp_nogeo add column AD_SERVER_ACTIVE_VIEW_MEASURABLE_IMPRESSIONS numeric default null;
alter table tercept_reports.dfp_nogeo add column AD_EXCHANGE_ACTIVE_VIEW_VIEWABLE_IMPRESSIONS numeric default null;
alter table tercept_reports.dfp_nogeo add column AD_EXCHANGE_ACTIVE_VIEW_MEASURABLE_IMPRESSIONS numeric default null;
alter table tercept_reports.dfp_nogeo add column AD_EXCHANGE_LINE_ITEM_LEVEL_TARGETED_IMPRESSIONS numeric default null;
alter table tercept_reports.dfp_nogeo add column AD_EXCHANGE_LINE_ITEM_LEVEL_TARGETED_CLICKS numeric default null;
alter table tercept_reports.dfp_nogeo add column AGGREGATED_DEMAND_CHANNEL VARCHAR(255) DEFAULT NULL;
alter table tercept_reports.dfp_nogeo add column TOTAL_ACTIVE_VIEW_VIEWABLE_IMPRESSIONS NUMBER default null;
alter table tercept_reports.dfp_nogeo add column TOTAL_ACTIVE_VIEW_MEASURABLE_IMPRESSIONS NUMBER default null;
alter table tercept_reports.dfp_nogeo add column TOTAL_LINE_ITEM_LEVEL_IMPRESSIONS NUMBER default null;
alter table tercept_reports.dfp_nogeo add column TOTAL_LINE_ITEM_LEVEL_CLICKS NUMBER default null;
alter table tercept_reports.dfp_nogeo add column TOTAL_LINE_ITEM_LEVEL_CPM_AND_CPC_REVENUE DECIMAL(20,10) default null;
alter table tercept_reports.dfp_nogeo add column DEMAND_CHANNEL_ID varchar(60) DEFAULT NULL;
alter table tercept_reports.dfp_nogeo alter column REQUESTED_AD_SIZES set DATA TYPE varchar(120);
alter table tercept_reports.dfp_nogeo add column YIELD_PARTNER varchar(80) DEFAULT NULL;
alter table tercept_reports.dfp_nogeo add column LINE_ITEM_TYPE varchar(50) DEFAULT NULL;


CREATE TABLE tercept_reports.cretio_hourly
(
    ACCOUNT_ID numeric(38,0) NOT NULL,
    CUSTOMER_ID numeric(38,0) NOT NULL,
    DATE date NOT NULL,
    HOUR time NOT NULL,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR varchar(20),
    PUBLISHER_ID numeric(38,0),
    PUBLISHER_NAME varchar(80),
    SITE_ID numeric(38,0),
    SITE_NAME varchar(120),
    PLACEMENT_ID numeric(38,0),
    PLACEMENT_NAME varchar(120),
    TOTAL_IMPRESSION numeric(38,0),
    IMPRESSION numeric(38,0),
    PASSBACK numeric(38,0),
    CLICKS numeric(38,0),
    TAKE_RATE numeric(20,10),
    CTR numeric(20,10),
    EARNINGS numeric(20,10),
    CPM numeric(20,10)
)
PARTITION BY (((cretio_hourly.ACCOUNT_ID * 100) + date_part('month', cretio_hourly.DATE)));

ALTER TABLE tercept_reports.cretio_hourly ADD COLUMN PARTNER_ID NUMBER NULL;
alter table tercept_reports.cretio_hourly add column COUNTRY varchar(120) default '-';

CREATE TABLE tercept_reports.creative
(
    DATE date NOT NULL,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR varchar(20),
    ACCOUNT_ID numeric(38,0) NOT NULL,
    CUSTOMER_ID numeric(38,0) NOT NULL,
    CREATIVE_PARTNER_ID numeric(38,0) DEFAULT NULL::numeric(1,0),
    CREATIVE_TYPE varchar(40) DEFAULT NULL,
    CREATIVE_ID numeric(38,0) DEFAULT NULL::numeric(1,0),
    CREATIVE_NAME varchar(255) DEFAULT NULL,
    CREATIVE_REQUEST numeric(38,0) DEFAULT NULL::numeric(1,0),
    AD_TAG_PARTNER_ID numeric(38,0) DEFAULT NULL::numeric(1,0),
    AD_TAG_ID varchar(50) DEFAULT NULL,
    AD_TAG_NAME varchar(120) DEFAULT NULL,
    AD_TAG_REQUEST numeric(38,0) DEFAULT NULL::numeric(1,0),
    CREATIVE_PARTNER_NAME varchar(40),
    AD_TAG_PARTNER_NAME varchar(40)
)
PARTITION BY (((creative.CUSTOMER_ID * 100) + date_part('month', creative.DATE)));


CREATE TABLE tercept_reports.partners
(
    PARTNER_NAME varchar(120) NOT NULL,
    PARTNER_ID numeric(38,0) NOT NULL,
    PARTNER_CODE varchar(20) NOT NULL
);
ALTER TABLE tercept_reports.partners ADD COLUMN partner_type INT NOT NULL DEFAULT 1;

CREATE TABLE tercept_reports.google_analytics
(
    CUSTOMER_ID numeric(38,0) NOT NULL,
    ACCOUNT_ID numeric(38,0) NOT NULL,
    INTEGRATION varchar(100) NOT NULL,
    YEAR numeric(38,0) NOT NULL,
    MONTH_INDEX numeric(38,0) NOT NULL,
    MONTH varchar(20) NOT NULL,
    WEEK varchar(20) NOT NULL,
    DATE date NOT NULL,
    USERS numeric(38,0) DEFAULT NULL::numeric(1,0),
    NEW_USERS numeric(38,0) DEFAULT NULL::numeric(1,0),
    SESSIONS numeric(38,0) DEFAULT NULL::numeric(1,0),
    SESSION_DURATION numeric(38,0) DEFAULT NULL::numeric(1,0),
    UNIQUE_DIMENSION_COMBINATIONS numeric(38,0) DEFAULT NULL::numeric(1,0),
    HITS numeric(38,0) DEFAULT NULL::numeric(1,0),
    UNIQUE_EVENTS numeric(38,0) DEFAULT NULL::numeric(1,0),
    PAGE_VIEWS numeric(38,0) DEFAULT NULL::numeric(1,0),
    TIME_ON_PAGE numeric(38,0) DEFAULT NULL::numeric(1,0),
    BOUNCES numeric(38,0) DEFAULT NULL::numeric(1,0),
    EVENT_CATEGORY varchar(120) DEFAULT NULL,
    EVENT_ACTION varchar(120) DEFAULT NULL,
    EVENT_LABEL varchar(255) DEFAULT NULL,
    SOURCE varchar(120) DEFAULT NULL,
    COUNTRY varchar(60) DEFAULT NULL,
    BOUNCE_RATE numeric(20,10) DEFAULT NULL::numeric(1,0),
    ENTRANCES numeric(38,0) DEFAULT NULL::numeric(1,0),
    HOUR time NOT NULL DEFAULT '23:00:00'::time,
    VIEW_ID varchar(30) DEFAULT NULL
)
PARTITION BY (((google_analytics.CUSTOMER_ID * 100) + date_part('month', google_analytics.DATE)));

ALTER TABLE tercept_reports.google_analytics  ALTER COLUMN DEL_YEAR DROP NOT NULL;
ALTER TABLE tercept_reports.google_analytics ADD COLUMN PARTNER_ID NUMBER NULL;
alter table tercept_reports.google_analytics add column VIEW_NAME varchar(255) default null;
alter table tercept_reports.google_analytics add column SOURCE_MEDIUM varchar(120) default null;
alter table tercept_reports.google_analytics add column SCREEN_NAME VARCHAR(255) default null;
alter table tercept_reports.google_analytics add column DEVICE_CATEGORY VARCHAR(80) default null;
alter table tercept_reports.google_analytics add column SCREEN_VIEWS NUMBER default null;
alter table tercept_reports.google_analytics add column TOTAL_EVENTS NUMBER default null;
alter table tercept_reports.google_analytics add column SCREEN_VIEWS NUMBER default null;

alter table tercept_reports.ga_segments add column VIEW_NAME varchar(255) default null;
alter table tercept_reports.google_analytics add column WF_REGION varchar(100) default null;

CREATE TABLE tercept_reports.facebook_insights
(
    CUSTOMER_ID numeric(38,0) NOT NULL,
    ACCOUNT_ID numeric(38,0) NOT NULL,
    DATE date NOT NULL,
    INTEGRATION varchar(100) NOT NULL,
    YEAR numeric(38,0) NOT NULL,
    MONTH_INDEX numeric(38,0) NOT NULL,
    MONTH varchar(20) NOT NULL,
    WEEK varchar(20) NOT NULL,
    COUNTRY varchar(20) DEFAULT NULL,
    ACCOUNT_CURRENCY varchar(20) DEFAULT NULL,
    AD_NAME varchar(255) DEFAULT NULL,
    ADSET_NAME varchar(255) DEFAULT NULL,
    ADSET_ID varchar(30) DEFAULT NULL,
    BUYING_TYPE varchar(120) DEFAULT NULL,
    CAMPAIGN_NAME varchar(255) DEFAULT NULL,
    CAMPAIGN_ID varchar(30) DEFAULT NULL,
    AD_ID varchar(30) DEFAULT NULL,
    OBJECTIVE varchar(50) DEFAULT NULL,
    ACCOUNT_NAME varchar(255) DEFAULT NULL,
    ADS_ACCOUNT_ID varchar(30) DEFAULT NULL,
    ACTIONS_TYPE varchar(255) DEFAULT NULL,
    CALL_TO_ACTION_CLICKS numeric(38,0) DEFAULT NULL::numeric(1,0),
    RELEVANCE_SCORE numeric(38,0) DEFAULT NULL::numeric(1,0),
    SOCIAL_SPEND float DEFAULT NULL::float,
    REACH numeric(38,0) DEFAULT NULL::numeric(1,0),
    SOCIAL_IMPRESSIONS numeric(38,0) DEFAULT NULL::numeric(1,0),
    FREQUENCY float DEFAULT NULL::float,
    SOCIAL_REACH numeric(38,0) DEFAULT NULL::numeric(1,0),
    UNIQUE_CLICKS numeric(38,0) DEFAULT NULL::numeric(1,0),
    CPC float DEFAULT NULL::float,
    SPEND float DEFAULT NULL::float,
    TOTAL_UNIQUE_ACTIONS numeric(38,0) DEFAULT NULL::numeric(1,0),
    SOCIAL_CLICKS numeric(38,0) DEFAULT NULL::numeric(1,0),
    CTR float DEFAULT NULL::float,
    CPM float DEFAULT NULL::float,
    CPP float DEFAULT NULL::float,
    UNIQUE_CTR float DEFAULT NULL::float,
    IMPRESSIONS numeric(38,0) DEFAULT NULL::numeric(1,0),
    CLICKS numeric(38,0) DEFAULT NULL::numeric(1,0),
    COST_PER_UNIQUE_CLICK float DEFAULT NULL::float,
    UNIQUE_ACTIONS_VALUE numeric(38,0) DEFAULT NULL::numeric(1,0),
    ACTIONS_VALUE numeric(38,0) DEFAULT NULL::numeric(1,0),
    HOUR time NOT NULL DEFAULT '23:00:00'::time
)
PARTITION BY (((facebook_insights.CUSTOMER_ID * 100) + date_part('month', facebook_insights.DATE)));


ALTER TABLE tercept_reports.facebook_insights ALTER COLUMN DEL_YEAR DROP NOT NULL;
ALTER TABLE tercept_reports.facebook_insights ADD COLUMN PARTNER_ID NUMBER NULL;

CREATE TABLE tercept_reports.facebook
(
    DATE date NOT NULL,
    CUSTOMER_ID numeric(38,0) NOT NULL,
    ACCOUNT_ID numeric(38,0) NOT NULL,
    INTEGRATION varchar(18) NOT NULL,
    WEEK varchar(20),
    MONTH varchar(20),
    MONTH_INDEX numeric(38,0),
    YEAR numeric(38,0),
    PARTNER_ID varchar(6) NOT NULL,
    COUNTRY varchar(40),
    DELIVERY_METHOD varchar(30),
    PLATFORM varchar(30),
    APP varchar(30),
    PLACEMENT varchar(30),
    DEAL varchar(30),
    PROPERTY varchar(30),
    FB_AD_NETWORK_REVENUE numeric(20,10),
    FB_AD_NETWORK_REQUEST numeric(38,0),
    FB_AD_NETWORK_CPM numeric(20,10),
    FB_AD_NETWORK_CLICK numeric(38,0),
    FB_AD_NETWORK_IMP numeric(38,0),
    FB_AD_NETWORK_FILLED_REQUEST numeric(38,0),
    FB_AD_NETWORK_FILL_RATE numeric(20,10),
    FB_AD_NETWORK_CTR numeric(20,10),
    FB_AD_NETWORK_SHOW_RATE numeric(20,10),
    FB_AD_NETWORK_VIDEO_GUARANTEE_REVENUE numeric(20,10),
    FB_AD_NETWORK_VIDEO_VIEW numeric(38,0),
    FB_AD_NETWORK_VIDEO_VIEW_RATE numeric(20,10),
    FB_AD_NETWORK_VIDEO_MRC numeric(38,0),
    FB_AD_NETWORK_VIDEO_MRC_RATE numeric(20,10),
    FB_AD_NETWORK_BIDDING_REQUEST numeric(38,0),
    FB_AD_NETWORK_BIDDING_RESPONSE numeric(38,0),
    PLACEMENT_VAR varchar(30) DEFAULT NULL,
    HOUR time NOT NULL DEFAULT '23:00:00'::time
)
PARTITION BY (((facebook.CUSTOMER_ID * 100) + date_part('month', facebook.DATE)));

ALTER TABLE tercept_reports.facebook  ALTER COLUMN DEL_YEAR DROP NOT NULL;
alter table tercept_reports.facebook add column WF_REGION varchar(40) default null;

CREATE TABLE tercept_reports.custom_cube
(
    CUSTOMER_ID numeric(38,0) NOT NULL,
    DATE date NOT NULL,
    YEAR numeric(38,0),
    MONTH_INDEX numeric(38,0),
    MONTH varchar(20),
    WEEK varchar(20),
    COUNTRY varchar(120) NOT NULL,
    AD_SERVER_CPM_AND_CPC_REVENUE numeric(20,10) DEFAULT NULL::numeric(1,0),
    AD_EXCHANGE_ESTIMATED_REVENUE numeric(20,10) DEFAULT NULL::numeric(1,0),
    ADSENSE_LINE_ITEM_LEVEL_REVENUE numeric(20,10) DEFAULT NULL::numeric(1,0),
    FB_AD_NETWORK_REVENUE numeric(20,10) DEFAULT NULL::numeric(1,0),
    FB_MARKETING_SPEND numeric(20,10) DEFAULT NULL::numeric(1,0),
    PAGE_VIEWS numeric(38,0) DEFAULT NULL::numeric(1,0)
)
PARTITION BY (((custom_cube.CUSTOMER_ID * 100) + date_part('month', custom_cube.DATE)));

ALTER TABLE tercept_reports.custom_cube ADD COLUMN AD_SERVER_IMPRESSIONS numeric(20,10) DEFAULT NULL::numeric(1,0);
ALTER TABLE tercept_reports.custom_cube ADD COLUMN AD_EXCHANGE_IMPRESSIONS numeric(20,10) DEFAULT NULL::numeric(1,0);
ALTER TABLE tercept_reports.custom_cube ADD COLUMN ADSENSE_LINE_ITEM_LEVEL_IMPRESSIONS numeric(20,10) DEFAULT NULL::numeric(1,0);
ALTER TABLE tercept_reports.custom_cube ADD COLUMN FB_AD_NETWORK_IMP numeric(20,10) DEFAULT NULL::numeric(1,0);
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN AD_SERVER_CPM_AND_CPC_REVENUE SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN AD_EXCHANGE_ESTIMATED_REVENUE SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN ADSENSE_LINE_ITEM_LEVEL_REVENUE SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN FB_AD_NETWORK_REVENUE SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN FB_MARKETING_SPEND SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN PAGE_VIEWS SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN AD_SERVER_IMPRESSIONS SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN AD_EXCHANGE_IMPRESSIONS SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN ADSENSE_LINE_ITEM_LEVEL_IMPRESSIONS SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN FB_AD_NETWORK_IMP SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN CRETIO_IMPRESSIONS SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN CRETIO_REVENUE SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN GA_PAGE_VIEWS_INSTANT_ARTICLES SET DEFAULT 0;
ALTER TABLE tercept_reports.custom_cube ALTER COLUMN GA_PAGE_VIEW_NON_INSTANT_ARTICLES SET DEFAULT 0;
alter table tercept_reports.custom_cube add column TOTAL_CODE_SERVED_COUNT numeric(38,0) DEFAULT 0;
alter table tercept_reports.custom_cube add column SESSIONS numeric(38,0) default null;
alter table tercept_reports.custom_cube add column AVG_SESSION_DURATION numeric(38,0) default null;

alter table tercept_reports.custom_cube add column BOUNCE_RATE numeric(38,0) default null;
alter table tercept_reports.custom_cube add column EXCHANGE_BIDDING_IMPRESSIONS NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column EXCHANGE_BIDDING_REVENUE NUMBER DEFAULT 0.0;
alter table tercept_reports.custom_cube add column MEDIATION_IMPRESSIONS NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column MEDIATION_REVENUE NUMBER DEFAULT 0.0;

alter table tercept_reports.custom_cube add column AD_EXCHANGE_IMPRESSIONS_APP NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column AD_EXCHANGE_IMPRESSIONS_WEB NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column AD_EXCHANGE_ESTIMATED_REVENUE_APP NUMBER DEFAULT 0.0;
alter table tercept_reports.custom_cube add column AD_EXCHANGE_ESTIMATED_REVENUE_WEB NUMBER DEFAULT 0.0;
alter table tercept_reports.custom_cube add column AD_SERVER_IMPRESSIONS_APP NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column AD_SERVER_IMPRESSIONS_WEB NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column AD_SERVER_CPM_AND_CPC_REVENUE_APP NUMBER DEFAULT 0.0;
alter table tercept_reports.custom_cube add column AD_SERVER_CPM_AND_CPC_REVENUE_WEB NUMBER DEFAULT 0.0;
alter table tercept_reports.custom_cube add column EXCHANGE_BIDDING_IMPRESSIONS_APP NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column EXCHANGE_BIDDING_IMPRESSIONS_WEB NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column EXCHANGE_BIDDING_REVENUE_APP NUMBER DEFAULT 0.0;
alter table tercept_reports.custom_cube add column EXCHANGE_BIDDING_REVENUE_WEB NUMBER DEFAULT 0.0;
alter table tercept_reports.custom_cube add column MEDIATION_IMPRESSIONS_APP NUMBER DEFAULT 0;
alter table tercept_reports.custom_cube add column MEDIATION_REVENUE_APP NUMBER DEFAULT 0.0;
alter table tercept_reports.custom_cube add column SCREEN_VIEWS NUMBER default null;

alter table tercept_reports.custom_cube add column GAANA_DEMAND_SOURCE varchar(100) DEFAULT NULL;
alter table tercept_reports.custom_cube add column GAANA_PLATFORM varchar(100) DEFAULT NULL;
alter table tercept_reports.custom_cube add column GAANA_BANNER_TYPE varchar(100) DEFAULT NULL;
alter table tercept_reports.custom_cube add column GAANA_SCREEN_NAME varchar(100) DEFAULT NULL;

CREATE TABLE tercept_reports.master_country
(
    STANDARD_NAME varchar(60) NOT NULL,
    ADX_NAME varchar(60) DEFAULT NULL,
    DFP_NAME varchar(60) DEFAULT NULL,
    PUB_NAME varchar(60) DEFAULT NULL,
    FAN_NAME varchar(60) DEFAULT NULL,
    FMA_NAME varchar(60) DEFAULT NULL,
    GA_NAME varchar(60) DEFAULT NULL
);

alter table tercept_reports.master_country add column CRI_NAME varchar(60) default null;


CREATE TABLE tercept_reports.accounts
(
    NAME varchar(120) NOT NULL,
    CUSTOMER_ID numeric(38,0) NOT NULL,
    ACCOUNT_ID numeric(38,0) NOT NULL,
    EXCHANGE_ID numeric(38,0) NOT NULL,
    PARTNER_ID varchar(10) NOT NULL,
    EMAIL varchar(30) DEFAULT NULL,
    CURRENCY varchar(10) NOT NULL,
    is_enabled boolean DEFAULT false,
    TIME_ZONE varchar(20) NOT NULL
);

ALTER TABLE tercept_reports.accounts ADD CONSTRAINT C_PRIMARY PRIMARY KEY (ACCOUNT_ID) DISABLED;



CREATE TABLE tercept_reports.header_bidding(
    CUSTOMER_ID numeric(38,0) NOT NULL,
    ACCOUNT_ID numeric(38,0) NOT NULL,
    INTEGRATION varchar(100) NOT NULL,
    YEAR numeric(38,0) NOT NULL,
    MONTH_INDEX numeric(38,0) NOT NULL,
    MONTH varchar(20) NOT NULL,
    WEEK varchar(20) NOT NULL,
    DATE date NOT NULL,
    HOUR time NOT NULL,
    VIEW_ID varchar(30) DEFAULT NULL,
    COUNTRY varchar(60) DEFAULT NULL,
    HOSTNAME varchar(80) DEFAULT NULL,
    EVENT_CATEGORY varchar(60) DEFAULT NULL,
    EVENT_LABEL varchar(60) DEFAULT NULL,
    BID_LOAD_TIME_TOTAL_EVENTS NUMBER DEFAULT 0,
    BIDS_TOTAL_EVENTS NUMBER DEFAULT 0,
    REQUESTS_TOTAL_EVENTS NUMBER DEFAULT 0,
    TIME_OUTS_TOTAL_EVENTS NUMBER DEFAULT 0,
    WINS_TOTAL_EVENTS NUMBER DEFAULT 0,
    BID_LOAD_TIME_EVENT_VALUE NUMBER DEFAULT 0,
    BIDS_EVENT_VALUE NUMBER DEFAULT 0,
    REQUESTS_EVENT_VALUE NUMBER DEFAULT 0,
    TIME_OUTS_EVENT_VALUE NUMBER DEFAULT 0,
    WINS_EVENT_VALUE NUMBER DEFAULT 0
)PARTITION BY (((header_bidding.CUSTOMER_ID * 100) + date_part('month', header_bidding.DATE)));


CREATE TABLE tercept_reports.google_adwords (
	ACCOUNT_ID NUMBER NOT NULL,
	CUSTOMER_ID NUMBER NOT NULL,
	PARTNER_ID NUMBER NOT NULL,
	INTEGRATION varchar(100) NOT NULL,
	DATE DATE NOT NULL,
	COUNTRY_CRITERIA_ID varchar(150) DEFAULT NULL,
	IMPRESSIONS NUMBER DEFAULT NULL,
	DEVICE NUMBER DEFAULT NULL,
	COST NUMERIC(20,10) DEFAULT NULL,
	CONVERSION_VALUE NUMERIC(20,10) DEFAULT NULL,
	CONVERSIONS NUMERIC(20,10) DEFAULT NULL,
	CLICKS NUMBER DEFAULT NULL
)PARTITION BY (((google_adwords.CUSTOMER_ID * 100) + date_part('month', google_adwords.DATE)));

alter table tercept_reports.google_adwords add column WEEK varchar(40) DEFAULT NULL;
alter table tercept_reports.google_adwords add column MONTH varchar(40) DEFAULT NULL;
alter table tercept_reports.google_adwords add column MONTH_INDEX NUMBER(4) DEFAULT NULL;
alter table tercept_reports.google_adwords add column YEAR NUMBER(6) DEFAULT NULL;


CREATE TABLE tercept_reports.dfm_header_bidding (
    ACCOUNT_ID NUMBER NOT NULL, 
    CUSTOMER_ID NUMBER NOT NULL, 
    PARTNER_ID varchar(30) NOT NULL, 
    INTEGRATION varchar(60) DEFAULT NULL, 
    YEAR varchar(10) DEFAULT NULL,    
    MONTH_INDEX NUMBER DEFAULT NULL, 
    MONTH varchar(30) DEFAULT NULL, 
    WEEK varchar(30) DEFAULT NULL, 
    NETWORK_NAME varchar(255) DEFAULT NULL, 
    DATE DATE NOT NULL, 
    DFP_IMPRESSIONS NUMBER DEFAULT NULL, 
    DFP_REVENUE FLOAT DEFAULT NULL, 
    NETWORK_IMPRESSIONS NUMBER DEFAULT NULL, 
    NETWORK_REVENUE FLOAT DEFAULT NULL, 
    TOTAL_PAGE_VIEWS NUMBER DEFAULT NULL, 
    IA_PAGE_VIEWS NUMBER DEFAULT NULL, 
    TOTAL_SESSIONS NUMBER DEFAULT NULL, 
    IA_SESSIONS NUMBER DEFAULT NULL
) PARTITION BY (((dfm_header_bidding.CUSTOMER_ID * 100) + date_part('month', dfm_header_bidding.DATE)));


CREATE TABLE tercept_reports.ab_testing_reports (
    CUSTOMER_ID numeric(38,0) NOT NULL, 
    ACCOUNT_ID numeric(38,0) NOT NULL, 
    INTEGRATION varchar(80) NOT NULL, 
    DATE date NOT NULL, 
    HOUR time NOT NULL, 
    WEEK varchar(20) NOT NULL, 
    MONTH varchar(20) NOT NULL, 
    MONTH_INDEX numeric(38,0) NOT NULL, 
    YEAR numeric(38,0) NOT NULL, 
    AD_EXCHANGE_BRANDING_TYPE varchar(30), 
    AD_EXCHANGE_TRANSACTION_TYPE varchar(20), 
    AD_EXCHANGE_PRICING_RULE_NAME varchar(80), 
    AD_EXCHANGE_DFP_AD_UNIT varchar(120), 
    CUSTOM_TARGETING_VALUE_ID numeric(38,0), 
    CUSTOM_TARGETING_KEY_ID numeric(38,0), 
    CUSTOM_TARGETING_KEY varchar(120), 
    CUSTOM_TARGETING_VALUE varchar(120), 
    AD_EXCHANGE_AD_REQUESTS numeric(38,0), 
    AD_EXCHANGE_ESTIMATED_REVENUE numeric(20,10)
) PARTITION BY (((ab_testing_reports.CUSTOMER_ID * 100) + date_part('month', ab_testing_reports.DATE)));

CREATE TABLE tercept_reports.gaana_adsense_report (
    CUSTOMER_ID numeric(38,0) NOT NULL,
    ACCOUNT_ID numeric(38,0) NOT NULL,
    PARTNER_ID varchar(16) NOT NULL,
    INTEGRATION varchar(100) NOT NULL,
    YEAR numeric(38,0) NOT NULL,
    MONTH_INDEX numeric(38,0) NOT NULL,
    MONTH varchar(20) NOT NULL,
    WEEK varchar(20) NOT NULL,
    DATE date NOT NULL,
    HOUR time NOT NULL,
    AD_UNIT_NAME varchar(116) DEFAULT NULL,
    AD_IMPRESSIONS NUMERIC(38,0) DEFAULT NULL,
    CLICKS NUMERIC(38,0) DEFAULT NULL,
    AD_IMPRESSIONS_RPM NUMERIC(20,10) DEFAULT NULL,
    EARNINGS NUMERIC(20,10) DEFAULT NULL,
    ACTIVE_VIEW_VIEWABLE_COUNT NUMERIC(20,10) DEFAULT 0
)PARTITION BY (((gaana_adsense_report.CUSTOMER_ID * 100) + date_part('month', gaana_adsense_report.DATE)));

CREATE TABLE tercept_reports.rtk_io( 
	CUSTOMER_ID numeric(38,0) NOT NULL,
	ACCOUNT_ID numeric(38,0) NOT NULL,
	INTEGRATION varchar(100) NOT NULL,
	PARTNER_ID varchar(10) NOT NULL,
	YEAR numeric(38,0) NOT NULL,
	MONTH_INDEX numeric(38,0) NOT NULL,
	MONTH varchar(20) NOT NULL,
	WEEK varchar(20) NOT NULL,
	DATE date NOT NULL,
	HOUR time NOT NULL,
	CAMPAIGN_ID varchar(30) DEFAULT NULL,
	AD_UNIT_NAME varchar(20) DEFAULT NULL,
	ORGANIZATION varchar(120) DEFAULT NULL,
	CPM DECIMAL(20,10),
	ECPM DECIMAL(20,10) DEFAULT NULL, 
	FILL_RATE DECIMAL(20,10) DEFAULT NULL,
	IMPRESSIONS_PAID DECIMAL(20,10) DEFAULT NULL,
	IMPRESSIONS_SAW NUMBER DEFAULT NULL,
	PROFIT DECIMAL(20,10) DEFAULT NULL,
	PUB_CPM DECIMAL(20,10) DEFAULT NULL,
	PUB_REVENUE DECIMAL(20,10) DEFAULT NULL,
	REVENUE DECIMAL(20,10) DEFAULT NULL
)
PARTITION BY (((rtk_io.CUSTOMER_ID * 100) + date_part('month', rtk_io.DATE)));


CREATE TABLE tercept_reports.rtk_io( 
	CUSTOMER_ID numeric(38,0) NOT NULL,
	ACCOUNT_ID numeric(38,0) NOT NULL,
	INTEGRATION varchar(100) NOT NULL,
	PARTNER_ID varchar(10) NOT NULL,
	YEAR numeric(38,0) NOT NULL,
	MONTH_INDEX numeric(38,0) NOT NULL,
	MONTH varchar(20) NOT NULL,
	WEEK varchar(20) NOT NULL,
	DATE date NOT NULL,
	HOUR time NOT NULL,
	WINNER_BIDDER varchar(80) DEFAULT NULL,
	COUNTRY varchar(200) DEFAULT NULL,
	DEVICE_CATEGORY varchar(80) DEFAULT NULL,
	CAMPAIGN_ID varchar(30) DEFAULT NULL,
	SITE varchar(200) DEFAULT NULL,
	CPM DECIMAL(20,10),
	WIN_RATE DECIMAL(20,10) DEFAULT NULL, 
	IMPRESSIONS NUMERIC(38) DEFAULT NULL,
	REVENUE DECIMAL(20,10) DEFAULT NULL
)
PARTITION BY (((rtk_io.CUSTOMER_ID * 100) + date_part('month', rtk_io.DATE)));


CREATE TABLE tercept_reports.wf_traffic( 
	CUSTOMER_ID numeric(38,0) NOT NULL,
	ACCOUNT_ID numeric(38,0) NOT NULL,
	INTEGRATION varchar(100) NOT NULL,
	PARTNER_ID varchar(10) NOT NULL,
	YEAR numeric(38,0) NULL,
	MONTH_INDEX numeric(38,0) NULL,
	MONTH varchar(20) NULL,
	WEEK varchar(20) NULL,
	DATE date NOT NULL,
	HOUR time NOT NULL DEFAULT '23:00:00'::time,
	COUNTRY_CODE varchar(120) DEFAULT NULL,
	CLICKS NUMBER DEFAULT NULL,
	EARNINGS DECIMAL(20,10) DEFAULT NULL,
	V9_CPM DECIMAL(20,10) DEFAULT NULL
);
alter table tercept_reports.wf_traffic add column WF_REGION varchar(20) default null;



CREATE TABLE tercept_reports.custom_dimensions (
  CUSTOMER_ID numeric(38,0) NOT NULL,
  ACCOUNT_ID numeric(38,0) NOT NULL,
  DIMENSION varchar(120) NOT NULL,
  MEMBER varchar(255) NOT NULL,
  CUSTOM_DIMENSION varchar(120) DEFAULT NULL,
  CUSTOM_MEMBER varchar(255) DEFAULT NULL,
  IS_ACTIVE boolean DEFAULT NULL
) PARTITION BY ((custom_dimensions.CUSTOMER_ID * 100));


CREATE TABLE tercept_reports.gaana_ctn( 
	CUSTOMER_ID numeric(38,0) NOT NULL,
	ACCOUNT_ID numeric(38,0) NOT NULL,
	INTEGRATION varchar(100) NOT NULL,
	PARTNER_ID varchar(10) NOT NULL,
	YEAR numeric(38,0) NULL,
	MONTH_INDEX numeric(38,0) NULL,
	MONTH varchar(20) NULL,
	WEEK varchar(20) NULL,
	DATE date NOT NULL,
	HOUR time NOT NULL DEFAULT '23:00:00'::time,
	AD_SLOT_NAME varchar(160) DEFAULT NULL,
	PLATFORM varchar(60) DEFAULT NULL,
	SITE_NAME varchar(80) DEFAULT NULL,
	FILLED_AD_REQUEST NUMERIC(38,0) DEFAULT NULL,
	IMPRESSIONS NUMERIC(38,0) DEFAULT NULL,
	ORG_CLICKS NUMERIC(38,0) DEFAULT NULL,
	PAID_CLICKS NUMERIC(38,0) DEFAULT NULL,
	SPENT DECIMAL(20,10) DEFAULT NULL,
	UNFILLED_AD_REQUESTS NUMERIC(38,0) DEFAULT NULL
)
PARTITION BY (date_part('month', gaana_ctn.DATE));

alter table tercept_reports.gaana_ctn add column LINE_ITEM_ID varchar(40) default null;
alter table tercept_reports.gaana_ctn add column ADVERTISER_NAME varchar(120) default null;
alter table tercept_reports.gaana_ctn add column PRICING_TYPE varchar(40) default null;
alter table tercept_reports.gaana_ctn add column PROPOSAL_TYPE varchar(80) default null;
alter table tercept_reports.gaana_ctn add column IS_ORGANIC NUMBER default null;
alter table tercept_reports.gaana_ctn add column AD_UNIT_ID varchar(40) default null;
alter table tercept_reports.gaana_ctn add column AD_UNIT_TYPE varchar(100) default null;
alter table tercept_reports.gaana_ctn add column GEO_NAME varchar(140) default null;
alter table tercept_reports.gaana_ctn add column CLICKS numeric(38,0) default null;


CREATE TABLE tercept_reports.fx_rates (
  SOURCE_CURRENCY varchar(6) NOT NULL,
  TARGET_CURRENCY varchar(6) NOT NULL,
  EXCHANGE_RATE decimal(20,10) DEFAULT NULL,
  DATE date NOT NULL
)
ORDER BY SOURCE_CURRENCY,TARGET_CURRENCY,DATE
PARTITION BY (date_part('month', fx_rates.DATE));


create table tercept_reports.sales_reports (
	CUSTOMER_ID numeric(38,0) NOT NULL,
	ACCOUNT_ID numeric(38,0) NOT NULL,
	INTEGRATION varchar(100) NOT NULL,
	PARTNER_ID varchar(10) NOT NULL,
	YEAR numeric(38,0) NULL,
	MONTH_INDEX numeric(38,0) NULL,
	MONTH varchar(20) NULL,
	WEEK varchar(20) NULL,
	DATE date NOT NULL,
	HOUR time NOT NULL DEFAULT '23:00:00'::time,
	RETAIL_PARTNER varchar(255) DEFAULT NULL,
	STORE_CODE varchar(140) DEFAULT NULL,
	STORE_NAME varchar(255) DEFAULT NULL,
	PRODUCT_STYLE_CODE varchar(140) DEFAULT NULL,
	PRODUCT_SIZE varchar(80) DEFAULT NULL,
	PRODUCT_COLOUR varchar(160) DEFAULT NULL,
	PRODUCT_EAN_CODE varchar(200) DEFAULT NULL,
	PRODUCT_QTY_SOLD numeric(38,0) DEFAULT 0,
	NET_SALES decimal(20,10) DEFAULT 0,
	GROSS_SALES decimal(20,10) DEFAULT 0
) PARTITION BY (((sales_reports.CUSTOMER_ID * 100) + date_part('month', sales_reports.DATE)));
