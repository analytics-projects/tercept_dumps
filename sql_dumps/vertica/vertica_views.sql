
/* CUREJOY Views */

CREATE OR REPLACE VIEW tercept_reports.DFP_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.dfp_nogeo where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADX_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.exchange_hourly where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADS_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.google_adsense_hourly where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.CRI_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.cretio_hourly where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.FAN_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.facebook where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.FMA_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.facebook_insights where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.GA_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.google_analytics where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.CUS_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.custom_cube where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.GA_SEGMENTS_CUREJOY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.ga_segments where customer_id=9 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);




/* WITTYFEED Views */

CREATE OR REPLACE VIEW tercept_reports.DFP_WITTYFEED AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR, (CASE WHEN COUNTRY_NAME='India' THEN 'IN' WHEN COUNTRY_NAME='United States' THEN 'US' WHEN COUNTRY_NAME='United Kingdom' THEN 'UK' WHEN COUNTRY_NAME='Australia' THEN 'AU' WHEN COUNTRY_NAME='Canada' THEN 'CA' ELSE 'Others' END) as WF_REGION  from tercept_reports.dfp_nogeo where customer_id=11 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 6, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADX_WITTYFEED AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR, (CASE WHEN COUNTRY_NAME='India' THEN 'IN' WHEN COUNTRY_NAME='United States' THEN 'US' WHEN COUNTRY_NAME='United Kingdom' THEN 'UK' WHEN COUNTRY_NAME='Australia' THEN 'AU' WHEN COUNTRY_NAME='Canada' THEN 'CA' ELSE 'Others' END) as WF_REGION  from tercept_reports.exchange_hourly where customer_id=11 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 6, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.FAN_WITTYFEED AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR, (CASE WHEN COUNTRY='IN' THEN 'IN' WHEN COUNTRY='US' THEN 'US' WHEN COUNTRY='GB' THEN 'UK' WHEN COUNTRY='AU' THEN 'AU' WHEN COUNTRY='CA' THEN 'CA' ELSE 'Others' END) as WF_REGION_1, (CASE WHEN PLACEMENT='1867008893561183' THEN 'New Ad Instant' WHEN PLACEMENT='1881465168782222' THEN 'Pub name- Recirculation Ad' WHEN PLACEMENT='2111397308885617' THEN 'Videogram_me' WHEN PLACEMENT='2067461523268918' THEN 'Videogram_com' WHEN PLACEMENT='2004073569617992' THEN 'Ex8_MM' WHEN PLACEMENT='2079283112086759' THEN 'Ex8.com' WHEN PLACEMENT='2116401398374930' THEN 'Story_com_ad1' WHEN PLACEMENT='2116402021708201' THEN 'Story_com_ad2' WHEN PLACEMENT='2127222733959463' THEN 'Story_com_mid' WHEN PLACEMENT='2171059879586026' THEN 'Story_me_ad1' WHEN PLACEMENT='2171060306252650' THEN 'Story_me_ad2' WHEN PLACEMENT='2184521368239877' THEN 'Story_me_mid' ELSE PLACEMENT END) as WF_PLACEMENT_NAME  from tercept_reports.facebook where customer_id=11 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 6, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.GA_WITTYFEED AS (
	SELECT a.*,ISNULL(b.CUSTOM_MEMBER, 'Unassigned') as WF_VIEW,concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.google_analytics a LEFT OUTER JOIN tercept_reports.custom_dimensions b ON a.SOURCE_MEDIUM = b.MEMBER where a.customer_id=11 and a.date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 6, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.CUS_WITTYFEED AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR, (CASE WHEN COUNTRY='India' THEN 'IN' WHEN COUNTRY='United States' THEN 'US' WHEN COUNTRY='United Kingdom' THEN 'UK' WHEN COUNTRY='Australia' THEN 'AU' WHEN COUNTRY='Canada' THEN 'CA' ELSE 'Others' END) as WF_REGION  from tercept_reports.custom_cube where customer_id=11 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 6, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.DFM_WITTYFEED AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.dfm_header_bidding where customer_id=11 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 6, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.RTK_WITTYFEED AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR, (CASE WHEN COUNTRY='IND' THEN 'IN' WHEN COUNTRY='USA' THEN 'US' WHEN COUNTRY='GBR' THEN 'UK' WHEN COUNTRY='AUS' THEN 'AU' WHEN COUNTRY='CAN' THEN 'CA' ELSE 'Others' END) as WF_REGION, (CASE WHEN CAMPAIGN_ID='23031' THEN 'story_me_ad1' WHEN CAMPAIGN_ID='23032' THEN 'story_me_ad2' WHEN CAMPAIGN_ID='23033' THEN 'story_com_ad1' WHEN CAMPAIGN_ID='23034' THEN 'story_com_ad2' WHEN CAMPAIGN_ID='23286' THEN 'story_me_mid' WHEN CAMPAIGN_ID='23282' THEN 'story_com_mid' ELSE 'unmapped-adunit' END) as AD_UNIT_NAME  from tercept_reports.rtk_io where customer_id=11 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 6, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.WFTFK_WITTYFEED AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.wf_traffic where customer_id=11 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 6, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);





/* QUIKR Views */

CREATE OR REPLACE VIEW tercept_reports.DFP_QUIKR AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.dfp_nogeo where customer_id=8 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADX_QUIKR AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.exchange_hourly where customer_id=8 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADS_QUIKR AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.google_adsense_hourly where customer_id=8 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);




/* EBAY Views */

CREATE OR REPLACE VIEW tercept_reports.DFP_EBAY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.dfp_nogeo where customer_id=6 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADX_EBAY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.exchange_hourly where customer_id=6 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADS_EBAY AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.google_adsense_hourly where customer_id=6 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 12, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);


/* GAANA Views */

CREATE OR REPLACE VIEW tercept_reports.FAN_GAANA AS (
	select a.*,concat(a.MONTH,concat('-',a.YEAR)) as MONTH_YEAR, (a.FB_AD_NETWORK_REVENUE*ISNULL(c.EXCHANGE_RATE,68)) as CONVERTED_FB_AD_NETWORK_REVENUE, b.PLACEMENT_NAME,b.PLATFORM_LABEL,b.BANNER_TYPE_LABEL,b.SCREEN_NAME_LABEL,b.AD_SERVER_LABEL from tercept_reports.facebook a LEFT JOIN tercept_reports.gaana_fan_placement_mapping b ON a.PLACEMENT=b.PLACEMENT_ID LEFT OUTER JOIN tercept_reports.fx_rates c ON a.DATE=c.DATE and c.SOURCE_CURRENCY='USD' and c.TARGET_CURRENCY='INR' where a.customer_id=12 and a.date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.GA_GAANA AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.google_analytics where customer_id=12 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.CUS_GAANA AS (
	SELECT a.*, concat(a.MONTH,concat('-',a.YEAR)) as MONTH_YEAR, (a.FB_AD_NETWORK_REVENUE*ISNULL(b.EXCHANGE_RATE,68)) as CONVERTED_FB_AD_NETWORK_REVENUE from tercept_reports.custom_cube a LEFT OUTER JOIN tercept_reports.fx_rates b ON a.DATE=b.DATE and b.SOURCE_CURRENCY='USD' and b.TARGET_CURRENCY='INR' where customer_id=12 and a.date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.DFP_GAANA AS (
	select a.*,concat(a.MONTH,concat('-',a.YEAR)) as MONTH_YEAR, b.PLATFORM_LABEL,b.BANNER_TYPE_LABEL,b.SCREEN_NAME_LABEL from tercept_reports.dfp_nogeo a LEFT JOIN tercept_reports.gaana_dfp_ad_unit_mapping b ON a.AD_UNIT_NAME=b.AD_UNIT_NAME where a.customer_id=12 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADX_GAANA AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.exchange_hourly where customer_id=12 and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADX_CTN_GAANA AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR from tercept_reports.exchange_hourly where customer_id=12 and DFP_AD_UNITS IN ('Test_Tracker_CTN » DFP_CTN_DUMMY_TRACKER » CTN_Gaana_iOS_ADX', 'Test_Tracker_CTN » DFP_CTN_DUMMY_TRACKER » CTN_Gaana_AOS_ADX') and date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

CREATE OR REPLACE VIEW tercept_reports.ADS_GAANA AS (
	select a.*,concat(a.MONTH,concat('-',a.YEAR)) as MONTH_YEAR, b.PLATFORM_LABEL,b.BANNER_TYPE_LABEL,b.SCREEN_NAME_LABEL from tercept_reports.google_adsense_hourly a LEFT JOIN tercept_reports.gaana_ads_ad_unit_mapping b ON a.AD_UNIT_NAME=b.AD_UNIT_NAME where a.customer_id=12 and a.date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);

/* Future Mapping */
/*CREATE OR REPLACE VIEW tercept_reports.CTN_GAANA AS (
	select a.*, concat(a.MONTH,concat('-',a.YEAR)) as MONTH_YEAR, b.REVENUE_TYPE as REVENUE_TYPE_LABEL, c.DEMAND_SOURCE as DEMAND_SOURCE_LABEL, d.PLATFORM_LABEL as GAANA_PLATFORM, d.BANNER_TYPE_LABEL, d.SCREEN_NAME_LABEL, d.AD_SERVER_LABEL from tercept_reports.gaana_ctn a LEFT OUTER JOIN tercept_reports.gaana_ctn_revenue_type_mapping b ON a.PROPOSAL_TYPE = b.PROPOSAL_TYPE and a.PRICING_TYPE=b.PRICING_TYPE LEFT OUTER JOIN tercept_reports.gaana_ctn_demand_source_mapping c ON a.ADVERTISER_NAME=c.ADVERTISER_NAME LEFT OUTER JOIN tercept_reports.gaana_ctn_ad_slot_mapping d ON a.AD_SLOT_NAME=d.AD_SLOT_NAME where customer_id=12 and a.date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);*/

/* Current Mapping */
CREATE OR REPLACE VIEW tercept_reports.CTN_GAANA AS (
	select a.*, concat(a.MONTH,concat('-',a.YEAR)) as MONTH_YEAR, b.REVENUE_TYPE as REVENUE_TYPE_LABEL, c.DEMAND_SOURCE as DEMAND_SOURCE_LABEL, d.PLATFORM_LABEL as GAANA_PLATFORM, d.BANNER_TYPE_LABEL, d.SCREEN_NAME_LABEL, d.AD_SERVER_LABEL from tercept_reports.gaana_ctn a LEFT OUTER JOIN tercept_reports.gaana_ctn_revenue_type_mapping b ON a.PROPOSAL_TYPE = b.PROPOSAL_TYPE LEFT OUTER JOIN tercept_reports.gaana_ctn_demand_source_mapping c ON a.ADVERTISER_NAME=c.ADVERTISER_NAME LEFT OUTER JOIN tercept_reports.gaana_ctn_ad_slot_mapping d ON a.AD_SLOT_NAME=d.AD_SLOT_NAME where customer_id=12 and a.date >= TIMESTAMPADD('m', DATEDIFF('m', TO_DATE('01-01-2010','MM-DD-YYYY'), CURRENT_DATE()) - 3, TO_DATE('01-01-2010','MM-DD-YYYY'))::date
);




/* MONROW Views */

CREATE OR REPLACE VIEW tercept_reports.CSR_MONROW AS (
	SELECT *, concat(MONTH,concat('-',YEAR)) as MONTH_YEAR, concat(RETAIL_PARTNER, concat(' - ', STORE_NAME)) AS STORE_FULL_NAME from tercept_reports.sales_reports where customer_id=13
);






/* Sanity Check */
select * from tercept_reports.DFP_CUREJOY LIMIT 1;
select * from tercept_reports.ADX_CUREJOY LIMIT 1;
select * from tercept_reports.ADS_CUREJOY LIMIT 1;
select * from tercept_reports.CRI_CUREJOY LIMIT 1;
select * from tercept_reports.FAN_CUREJOY LIMIT 1;
select * from tercept_reports.FMA_CUREJOY LIMIT 1;
select * from tercept_reports.GA_CUREJOY LIMIT 1;
select * from tercept_reports.CUS_CUREJOY LIMIT 1;
select * from tercept_reports.GA_SEGMENTS_CUREJOY LIMIT 1;

select * from tercept_reports.DFP_WITTYFEED LIMIT 1;
select * from tercept_reports.ADX_WITTYFEED LIMIT 1;
select * from tercept_reports.FAN_WITTYFEED LIMIT 1;
select * from tercept_reports.GA_WITTYFEED LIMIT 1;
select * from tercept_reports.CUS_WITTYFEED LIMIT 1;
select * from tercept_reports.DFM_WITTYFEED LIMIT 1;
select * from tercept_reports.RTK_WITTYFEED LIMIT 1;
select * from tercept_reports.WFTFK_WITTYFEED LIMIT 1;

select * from tercept_reports.DFP_QUIKR LIMIT 1;
select * from tercept_reports.ADX_QUIKR LIMIT 1;
select * from tercept_reports.ADS_QUIKR LIMIT 1;

select * from tercept_reports.DFP_EBAY LIMIT 1;
select * from tercept_reports.ADX_EBAY LIMIT 1;
select * from tercept_reports.ADS_EBAY LIMIT 1;

select * from tercept_reports.FAN_GAANA LIMIT 1;
select * from tercept_reports.GA_GAANA LIMIT 1;
select * from tercept_reports.CUS_GAANA LIMIT 1;
select * from tercept_reports.DFP_GAANA LIMIT 1;
select * from tercept_reports.ADX_GAANA LIMIT 1;
select * from tercept_reports.ADX_CTN_GAANA LIMIT 1;
select * from tercept_reports.ADS_GAANA LIMIT 1;
select * from tercept_reports.CTN_GAANA LIMIT 1;

select * from tercept_reports.CSR_MONROW LIMIT 1;





/* Dates Check */

select year,month from tercept_reports.DFP_CUREJOY group by year,month order by year,month;

select year,month from tercept_reports.ADX_WITTYFEED group by year,month order by year,month;

select year,month from tercept_reports.ADX_QUIKR group by year,month order by year,month;

select year,month from tercept_reports.ADX_EBAY group by year,month order by year,month;

select year,month from tercept_reports.ADS_GAANA group by year,month order by year,month;


select * from tercept_reports.CSR_MONROW LIMIT 1;