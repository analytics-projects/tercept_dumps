/*
SQLyog Community
MySQL - 5.7.20-log : Database - tercept_reports
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tercept_reports` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `tercept_reports`;

/*Table structure for table `download_currencies` */

DROP TABLE IF EXISTS `download_currencies`;

CREATE TABLE `download_currencies` (
  `from_curr` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `to_curr` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `download_currencies` */

insert  into `download_currencies`(`from_curr`,`to_curr`,`start_date`,`end_date`,`is_active`) values 
('USD','INR','2018-06-01','2018-06-18',1),

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
